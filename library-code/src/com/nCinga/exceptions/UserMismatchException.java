package com.nCinga.exceptions;

public class UserMismatchException extends RuntimeException {

    public UserMismatchException(String msg){
        super(msg);
    }

}
