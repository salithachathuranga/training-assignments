package com.nCinga.exceptions;

public class AvailableBookCountException extends RuntimeException {

    public AvailableBookCountException(String msg){
        super(msg);
    }

}
