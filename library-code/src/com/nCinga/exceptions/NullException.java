package com.nCinga.exceptions;

public class NullException extends RuntimeException {

    public NullException(String msg){
        super(msg);
    }
}
