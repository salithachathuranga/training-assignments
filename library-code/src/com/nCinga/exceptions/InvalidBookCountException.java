package com.nCinga.exceptions;

public class InvalidBookCountException extends  RuntimeException{

    public InvalidBookCountException(String msg){
        super(msg);
    }

}
