package com.nCinga.bo;

public class Admin {

    private String name;
    private String idNo;

    public Admin(String name, String idNo) {
        setIdNo(idNo);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }
}
