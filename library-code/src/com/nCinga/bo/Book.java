package com.nCinga.bo;

import com.nCinga.enums.Subject;
import com.nCinga.exceptions.NullException;

public class Book {

    private String name;
    private Subject subject;
    private int totalCount;
    private int issuedCount;

    public Book(String name, Subject subject, int totalCount, int issuedCount) {
        if(name == null || subject == null) {
            throw new NullException("name or subject can not be null");
        }
        setName(name);
        setSubject(subject);
        setTotalCount(totalCount);
        setIssuedCount(issuedCount);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        if(totalCount >= 0) {
            this.totalCount = totalCount;
        }
        else {
            this.totalCount = 0;
        }
    }

    public int getIssuedCount() {
        return issuedCount;
    }

    public void setIssuedCount(int issuedCount) {
        if(issuedCount >= 0) {
            this.issuedCount = issuedCount;
        }
        else {
            this.issuedCount = 0;
        }
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", totalCount=" + totalCount +
                ", issuedCount=" + issuedCount +
                '}';
    }
}
