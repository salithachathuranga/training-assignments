package com.nCinga.bo;

public class IssuedBook {

    private Book book;
    private Student student;
    private Admin admin;

    public IssuedBook(Book book, Student student, Admin admin) {
        setBook(book);
        setStudent(student);
        setAdmin(admin);
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

}

