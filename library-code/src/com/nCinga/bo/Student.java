package com.nCinga.bo;

import com.nCinga.enums.Degree;

public class Student {

    private String rollNo;
    private String name;
    private String batch;
    private String section;
    private Degree degree;
    private int borrowedBookCount;

    public Student(String rollNo, String name, String batch, String section, Degree degree) {
        setRollNo(rollNo);
        setName(name);
        setBatch(batch);
        setSection(section);
        setDegree(degree);
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public int getBorrowedBookCount() {
        return borrowedBookCount;
    }

    public void setBorrowedBookCount(int borrowedBookCount) {
        this.borrowedBookCount = borrowedBookCount;
    }

    @Override
    public String toString() {
        return "Student{" +
                "rollNo='" + rollNo + '\'' +
                ", name='" + name + '\'' +
                ", borrowedBookCount=" + borrowedBookCount +
                '}';
    }
}
