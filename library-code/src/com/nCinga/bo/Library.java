package com.nCinga.bo;

import com.nCinga.enums.Degree;
import com.nCinga.exceptions.AvailableBookCountException;
import com.nCinga.exceptions.InvalidBookCountException;
import com.nCinga.exceptions.UserMismatchException;

public class Library {

    private Book[] books;

    public Library(Book[] books) {
        setBooks(books);
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public IssuedBook issueBookService(Book book, Student student, Admin admin){

        IssuedBook booking;

        if((book.getTotalCount()-book.getIssuedCount()) == 0){
            throw new AvailableBookCountException("no available copies of this book");
        }
        else {
            if (student.getDegree() == Degree.B_TECH && student.getBorrowedBookCount() >= 5){
                throw new InvalidBookCountException("invalid count for B_TECH");
            }
            else if(student.getDegree() == Degree.M_TECH && student.getBorrowedBookCount() >= 10){
                throw new InvalidBookCountException("invalid count for M_TECH");
            }
            else {
                book.setIssuedCount(book.getIssuedCount()+1);
                booking = new IssuedBook(book,student,admin);
                student.setBorrowedBookCount(student.getBorrowedBookCount()+1);
            }
        }
        System.out.print("Booking Details: ");
        System.out.print(booking.getStudent());
        System.out.print(" | ");
        System.out.println(booking.getBook());
        return booking;
    }

    public void returnBookService(Book book, Student student, IssuedBook booking){
        int availableBookCount = book.getTotalCount() - book.getIssuedCount();
        if(!booking.getStudent().getRollNo().equals(student.getRollNo())){
            throw new UserMismatchException("user not matched");
        }
        else if(book.getTotalCount() < availableBookCount+1){
            throw new InvalidBookCountException("total book count exceeded");
        }
        else {
            book.setIssuedCount(book.getIssuedCount()-1);
            System.out.println("Issued Book Count for book - "+book.getName()+" : "+book.getIssuedCount());
            student.setBorrowedBookCount(student.getBorrowedBookCount()-1);
            System.out.println("Borrowed Book Count for - "+student.getName()+" : "+student.getBorrowedBookCount());
        }
    }

}
