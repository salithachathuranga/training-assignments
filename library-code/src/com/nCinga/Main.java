package com.nCinga;

import com.nCinga.bo.*;
import com.nCinga.enums.Degree;
import com.nCinga.enums.Subject;

public class Main {

    public static void main(String[] args) {

        Book[] books = new Book[5];
        Book book1 = new Book("Concepts of Biology", Subject.BIO, 50, 10);
        Book book2 = new Book("Basic Maths", Subject.MATHS, 10, 1);
        books[0] = book1;
        books[1] = book2;

        Admin admin = new Admin("admin", "111");

        Student student1 = new Student("123", "salitha", "2020", "IT", Degree.B_TECH);
        Student student2 = new Student("456", "yasas", "2020", "IT", Degree.M_TECH);

        Library library = new Library(books);

        IssuedBook booking1 = library.issueBookService(book1,student1,admin);
        IssuedBook booking2 = library.issueBookService(book2,student1,admin);
        IssuedBook booking3 = library.issueBookService(book1,student1,admin);
        IssuedBook booking4 = library.issueBookService(book2,student1,admin);
        IssuedBook booking5 = library.issueBookService(book1,student1,admin);
        IssuedBook booking6 = library.issueBookService(book2,student1,admin);

        library.returnBookService(book1,student1,booking1);

    }
}
