package com.nCinga.service.impl;

import com.nCinga.bo.*;
import com.nCinga.dao.IssueBookRecordDao;
import com.nCinga.dao.LibraryBookRecordDao;
import com.nCinga.dao.LibraryDao;
import com.nCinga.service.LibraryRepositoryService;

import java.util.List;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {

    private LibraryDao libraryDao;
    private IssueBookRecordDao issueBookRecordDao;
    private LibraryBookRecordDao libraryBookRecordDao;

    public LibraryRepositoryServiceImpl(LibraryDao libraryDao, IssueBookRecordDao issueBookRecordDao, LibraryBookRecordDao libraryBookRecordDao) {
        setLibraryDao(libraryDao);
        setLibraryBookRecordDao(libraryBookRecordDao);
        setIssueBookRecordDao(issueBookRecordDao);
    }

    public LibraryDao getLibraryDao() {
        return libraryDao;
    }

    public void setLibraryDao(LibraryDao libraryDao) {
        this.libraryDao = libraryDao;
    }

    public IssueBookRecordDao getIssueBookRecordDao() {
        return issueBookRecordDao;
    }

    public void setIssueBookRecordDao(IssueBookRecordDao issueBookRecordDao) {
        this.issueBookRecordDao = issueBookRecordDao;
    }

    public LibraryBookRecordDao getLibraryBookRecordDao() {
        return libraryBookRecordDao;
    }

    public void setLibraryBookRecordDao(LibraryBookRecordDao libraryBookRecordDao) {
        this.libraryBookRecordDao = libraryBookRecordDao;
    }

    public IssuedBookRecord issueBook(Book book, Student student, Admin admin, Library library) {
        return null;
    }
}
