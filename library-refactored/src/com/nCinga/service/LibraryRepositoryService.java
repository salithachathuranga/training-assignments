package com.nCinga.service;

import com.nCinga.bo.*;

public interface LibraryRepositoryService {

    IssuedBookRecord issueBook(Book book, Student student, Admin admin, Library library);
}
