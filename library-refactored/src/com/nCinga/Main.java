package com.nCinga;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Book;
import com.nCinga.bo.Library;
import com.nCinga.bo.LibraryBookRecord;
import com.nCinga.bo.enums.Subject;
import com.nCinga.dao.AdminDao;
import com.nCinga.dao.IssueBookRecordDao;
import com.nCinga.dao.LibraryBookRecordDao;
import com.nCinga.dao.LibraryDao;
import com.nCinga.service.LibraryRepositoryService;
import com.nCinga.service.impl.LibraryRepositoryServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        AdminDao adminDao = new AdminDao();
        Admin admin = new Admin("admin", 123);
        adminDao.addAdmin(admin);

        Book book1 = new Book("Molecular Biology", Subject.BIO);
        Library library1 = new Library(1, "Bio Library");
        LibraryBookRecord record1 = new LibraryBookRecord(book1,library1, 10, 5);
//        List<LibraryBookRecord> libraryBookRecords = new ArrayList<>();
//        libraryBookRecords.add(record1);
//        List<Library> libraries = new ArrayList<>();

        LibraryDao libraryDao = new LibraryDao();

        LibraryBookRecordDao libraryBookRecordDao = new LibraryBookRecordDao();
        libraryBookRecordDao.addLibraryBookRecord(record1);

        IssueBookRecordDao issueBookRecordDao = new IssueBookRecordDao();

        LibraryRepositoryService service = new LibraryRepositoryServiceImpl(libraryDao, issueBookRecordDao, libraryBookRecordDao);


    }
}
