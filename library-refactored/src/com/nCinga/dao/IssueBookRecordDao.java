package com.nCinga.dao;

import com.nCinga.bo.IssuedBookRecord;
import com.nCinga.bo.LibraryBookRecord;

import java.util.ArrayList;
import java.util.List;

public class IssueBookRecordDao {

    private List<IssuedBookRecord> issuedBookRecords;

    public IssueBookRecordDao() {
        this.issuedBookRecords = new ArrayList<>();
    }

    public void addLibraryBookRecord(IssuedBookRecord record){
        this.issuedBookRecords.add(record);
    }

    public void removeLibraryBookRecord(IssuedBookRecord record){
        this.issuedBookRecords.remove(record);
    }

    public boolean isIssuedBookRecordExists(IssuedBookRecord record){
        if(issuedBookRecords.contains(record)){
            return true;
        }
        else {
            return false;
        }
    }
}
