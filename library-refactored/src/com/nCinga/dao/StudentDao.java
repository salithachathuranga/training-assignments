package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Library;
import com.nCinga.bo.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {

    List<Student> students;

    public StudentDao(List<Student> students) {
        this.students = new ArrayList<>();
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student){
        this.students.add(student);
    }

    public void removeStudent(Student student){
        this.students.remove(student);
    }

    public Student findStudentById(int id){
        for (Student student : students) {
            if(student.getRollNo() == id){
                return student;
            }
        }
        return null;
    }

    public Student findStudentByName(String name){
        for (Student student : students) {
            if(student.getName().equals(name)){
                return student;
            }
        }
        return null;
    }

    public boolean isStudentExists(Student student){
        if(students.contains(student)){
            return true;
        }
        else {
            return false;
        }
    }

    public void updateStudentName(int id, String newName){
        Student student = findStudentById(id);
        student.setName(newName);
    }
}
