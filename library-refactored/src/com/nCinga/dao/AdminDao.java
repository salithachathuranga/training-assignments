package com.nCinga.dao;

import com.nCinga.bo.Admin;

import java.util.ArrayList;
import java.util.List;

public class AdminDao {

    private List<Admin> admins;

    public AdminDao() {
        admins = new ArrayList<>();
    }

    public List<Admin> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Admin> admins) {
        this.admins = admins;
    }

    public void addAdmin(Admin admin){
        admins.add(admin);
    }

    public void removeAdmin(Admin admin){
        admins.remove(admin);
    }

    public Admin findAdminById(int id){
        for (Admin admin : admins) {
            if(admin.getId() == id){
                return admin;
            }
        }
        return null;
    }

    public Admin findAdminByName(String name){
        for (Admin admin : admins) {
            if(admin.getName().equals(name)){
                return admin;
            }
        }
        return null;
    }

    public boolean isAdminExists(Admin admin){
        if(admins.contains(admin)){
            return true;
        }
        else {
            return false;
        }
    }

    public void updateAdminName(int id, String newName){
        Admin admin = findAdminById(id);
        admin.setName(newName);
    }

}
