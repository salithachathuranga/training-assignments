package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.bo.LibraryBookRecord;

import java.util.ArrayList;
import java.util.List;

public class LibraryBookRecordDao {

    private List<LibraryBookRecord> bookRecords;

    public LibraryBookRecordDao() {
         this.bookRecords = new ArrayList<>();
    }

    public List<LibraryBookRecord> getBookRecords() {
        return bookRecords;
    }

    public void setBookRecords(List<LibraryBookRecord> bookRecords) {
        this.bookRecords = bookRecords;
    }

    public void addLibraryBookRecord(LibraryBookRecord record){
        this.bookRecords.add(record);
    }

    public void removeLibraryBookRecord(LibraryBookRecord record){
        this.bookRecords.remove(record);
    }

    public boolean isLibraryBookRecordExists(LibraryBookRecord record){
        if(bookRecords.contains(record)){
            return true;
        }
        else {
            return false;
        }
    }
}
