package com.nCinga.dao;

import com.nCinga.bo.IssuedBookRecord;
import com.nCinga.bo.Library;
import com.nCinga.bo.Student;

import java.util.ArrayList;
import java.util.List;

public class LibraryDao {

    private List<Library> libraries;

    public LibraryDao() {
        this.libraries = new ArrayList<>();
    }

    public void addLibrary(Library library){
        this.libraries.add(library);
    }

    public void removeLibrary(Library library){
        this.libraries.remove(library);
    }

    public Library findLibraryById(int id){
        for (Library library : libraries) {
            if(library.getLibraryId() == id){
                return library;
            }
        }
        return null;
    }

    public Library findLibraryByName(String name){
        for (Library library : libraries) {
            if(library.getLibraryName().equals(name)){
                return library;
            }
        }
        return null;
    }

    public boolean isLibraryExists(Library library){
        if(libraries.contains(library)){
            return true;
        }
        else {
            return false;
        }
    }

    public void updateLibraryName(int id, String newName){
        Library library = findLibraryById(id);
        library.setLibraryName(newName);
    }
}
