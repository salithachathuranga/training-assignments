package com.nCinga.bo;

import java.util.Objects;

public class LibraryBookRecord {

    private Book book;
    private Library library;
    private int totalBookCount;
    private int issuedBookCount;

    public LibraryBookRecord(Book book, Library library, int totalBookCount, int issuedBookCount) {
        setBook(book);
        setLibrary(library);
        setTotalBookCount(totalBookCount);
        setIssuedBookCount(issuedBookCount);
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public int getTotalBookCount() {
        return totalBookCount;
    }

    public void setTotalBookCount(int totalBookCount) {
        this.totalBookCount = totalBookCount;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }

    public void setIssuedBookCount(int issuedBookCount) {
        this.issuedBookCount = issuedBookCount;
    }

    public String toString() {
        return "LibraryBookRecord{book=" + book + ", library=" + library + ", totalBookCount=" + totalBookCount + ", issuedBookCount=" + issuedBookCount + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryBookRecord that = (LibraryBookRecord) o;
        return Objects.equals(book, that.book) && Objects.equals(library, that.library);
    }

    public int hashCode() {
        return Objects.hash(book, library);
    }
}
