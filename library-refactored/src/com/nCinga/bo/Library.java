package com.nCinga.bo;

import java.util.Objects;

public class Library {

    private int libraryId;
    private String libraryName;

    public Library(int libraryId, String libraryName) {
        setLibraryId(libraryId);
        setLibraryName(libraryName);
    }

    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    @Override
    public String toString() {
        return "Library{libraryId=" + libraryId + ", libraryName=" + libraryName + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Library library = (Library) o;
        return libraryId == library.libraryId && libraryName.equals(library.libraryName);
    }

    public int hashCode() {
        return Objects.hash(libraryId, libraryName);
    }
}
