package com.nCinga.bo;

import com.nCinga.bo.enums.Subject;

import java.util.Objects;

public class Book {

    private String name;
    private Subject subject;

    public Book(String name, Subject subject) {
        setName(name);
        setSubject(subject);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String toString() {
        return "Book{name=" + name + ", subject=" + subject + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return name.equals(book.name) && subject == book.subject;
    }

    public int hashCode() {
        return Objects.hash(name, subject);
    }
}
