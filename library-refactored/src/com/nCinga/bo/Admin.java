package com.nCinga.bo;

import java.util.Objects;

public class Admin {

    private String name;
    private int id;

    public Admin(String name, int id) {
        setName(name);
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        return "Admin{name=" + name + ", id=" + id + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Admin admin = (Admin) o;
        return id == admin.id && name.equals(admin.name);
    }

    public int hashCode() {
        return Objects.hash(name, id);
    }
}
