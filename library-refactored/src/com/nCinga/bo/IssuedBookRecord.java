package com.nCinga.bo;

import java.util.Objects;

public class IssuedBookRecord {

    private int issueId;
    private Student student;
    private Book book;
    private Admin admin;

    public IssuedBookRecord(Student student, Book book, Admin admin) {
       setStudent(student);
       setBook(book);
       setAdmin(admin);
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String toString() {
        return "IssuedBookRecord{student=" + student + ", book=" + book + ", admin=" + admin + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return Objects.equals(student, that.student) && Objects.equals(book, that.book) && Objects.equals(admin, that.admin);
    }

    public int hashCode() {
        return Objects.hash(student, book, admin);
    }
}
