package com.nCinga.bo;

import com.nCinga.bo.enums.Degree;

import java.util.Objects;

public class Student {

    private int rollNo;
    private String name;
    private String batch;
    private String section;
    private Degree degree;

    public Student(int rollNo, String name, String batch, String section, Degree degree) {
        setRollNo(rollNo);
        setName(name);
        setBatch(batch);
        setSection(section);
        setDegree(degree);
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public String toString() {
        return "Student{rollNo=" + rollNo + ", name=" + name + ", batch=" + batch + ", section=" + section + ", degree=" + degree + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return rollNo == student.rollNo && name.equals(student.name);
    }

    public int hashCode() {
        return Objects.hash(rollNo, name);
    }
}
