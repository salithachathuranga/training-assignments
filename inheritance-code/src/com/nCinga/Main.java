package com.nCinga;

public class Main {

    public static void main(String[] args) {

        System.out.println("Cutting Class");

        Cutting cutting1 = new Cutting();
        Cutting cutting2 = new Cutting(15);
        cutting1.increaseProductCount();
        cutting1.printCountOfProduct();
        cutting1.decreaseProductCount();
        cutting1.printCountOfProduct();
        cutting2.increaseProductCount();
        cutting2.printCountOfProduct();
        cutting2.decreaseProductCount();
        cutting2.printCountOfProduct();

        System.out.println("Sewing Class");

        Sewing swing1 = new Sewing();
        Sewing swing2 = new Sewing(15);
        swing1.increaseProductCount();
        swing1.printCountOfProduct();
        swing1.decreaseProductCount();
        swing1.printCountOfProduct();
        swing2.increaseProductCount();
        swing2.printCountOfProduct();
        swing2.decreaseProductCount();
        swing2.printCountOfProduct();

        System.out.println("Packing Class");

        Packing packing1 = new Packing();
        Packing packing2 = new Packing(15);
        packing1.increaseProductCount();
        packing1.printCountOfProduct();
        packing1.decreaseProductCount();
        packing1.printCountOfProduct();
        packing2.increaseProductCount();
        packing2.printCountOfProduct();
        packing2.decreaseProductCount();
        packing2.printCountOfProduct();

    }
}
