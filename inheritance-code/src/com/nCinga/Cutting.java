package com.nCinga;

public class Cutting implements Department {

    private int countOfProducts;

    public Cutting(){
        this.countOfProducts = 0;
    }

    public Cutting(int count){
        this.countOfProducts = count;
    }

    private int setCountOfProducts(int count){
        if(count > 0){
            this.countOfProducts = count;
        }
        else {
            this.countOfProducts = 0;
        }
        return this.countOfProducts;
    }

    public int getCuttingCountOfProducts(){
        return this.countOfProducts;
    }

    public int increaseProductCount() {
        int count = this.countOfProducts+1;
        return setCountOfProducts(count);
    }

    public int decreaseProductCount() {
        int count = this.countOfProducts-1;
        return setCountOfProducts(count);
    }

    public void printCountOfProduct() {
        System.out.println("Cutting Product Count: "+ getCuttingCountOfProducts());
    }
}
